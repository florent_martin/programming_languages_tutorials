/* This script allows to display a fixed top menu bar.
   I chosed to use a class for menu, but an id might be better.
   */

document.write("<div  class=\"menu\">");
document.write("<a href=\"index.html\">Home</a>");
document.write("<a href=\"basics.html\">Basics</a>");
document.write("<a href=\"dataframes.html\">Dataframes</a>");
document.write("<a href=\"date.html\">Date</a>");
document.write("<a href=\"data_science.html\">Data Science</a>");
document.write("<a href=\"mathematics.html\">Mathematics</a>");
document.write("<a href=\"models.html\">Models</a>");
document.write("<a href=\"packages.html\">Packages</a>");
document.write("<a href=\"plot.html\">Plot </a>");
document.write("<a href=\"statistics.html\">Statistics</a>");
document.write("<\/div>");

//document.write("<li class='active'><a href='home.html'><span>Home<\/span><\/a><\/li>");

/* <div class="topnav">
   <a class="active" href="#home">Home</a>
   <a href="#news">News</a>
   <a href="#contact">Contact</a>
   <a href="#about">About</a>
   </div>
   */
