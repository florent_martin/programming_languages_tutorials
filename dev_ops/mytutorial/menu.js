/* This script allows to display a fixed top menu bar.
   I chosed to use a class for menu, but an id might be better.
   */


/* Menu for the DevOps Section */

document.write("<div  class=\"menu\">");
document.write("<a href=\"index.html\">Home</a>");
document.write("<a href=\"atom.html\">Atom</a>");
document.write("<a href=\"basics.html\">Basics</a>");
document.write("<a href=\"code_formatting.html\">Code formatting</a>");
document.write("<a href=\"compression.html\">Compression</a>");
document.write("<a href=\"docker.html\">Docker</a>");
document.write("<a href=\"firewall.html\">Firewall</a>");
document.write("<a href=\"flask.html\">Flask</a>");
document.write("<a href=\"multiplexers.html\">Multiplexers</a>");
document.write("<a href=\"profiler.html\">Profiler</a>");
document.write("<a href=\"pycharm.html\">PyCharm</a>");
document.write("<a href=\"virtualenv.html\">Virtualenv</a>");
document.write("<\/div>");
