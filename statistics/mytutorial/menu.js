/* This script allows to display a fixed top menu bar.
   I chosed to use a class for menu, but an id might be better.
   */

document.write("<div  class=\"menu\">");
document.write("<a href=\"basics.html\">Basics</a>");
document.write("<a href=\"bayesian_statistics.html\">Bayesian statistics</a>");
document.write("<a href=\"local_search.html\">Local Search</a>");
document.write("<a href=\"statsmodels.html\">Statsmodels</a>");
document.write("<\/div>");
