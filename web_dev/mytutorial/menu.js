/* This script allows to display a fixed top menu bar.
   I chosed to use a class for menu, but an id might be better.
   */

document.write("<div  class=\"menu\">");
document.write("<a href=\"basics.html\">Basics</a>");
document.write("<a href='bootstrap.html'>Bootstrap</a>");
document.write("<a href=\"css.html\">CSS</a>");
document.write("<a href=\"flask.html\">Flask</a>");
document.write("<a href=\"html.html\">Html</a>");
document.write("<a href=\"http_server.html\">HTTP server</a>");
document.write("<a href=\"sass_scss.html\">Sass SCSS</a>");
document.write("<\/div>");
