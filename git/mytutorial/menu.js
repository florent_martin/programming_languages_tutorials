/* This script allows to display a fixed top menu bar.
   I chosed to use a class for menu, but an id might be better.
   */

document.write("<div  class=\"menu\">");
document.write("<a href=\"index.html\">Home</a>");
document.write("<a href=\"basics.html\">Basics</a>");
document.write("<a href=\"best_practices.html\">Best practices</a>");
document.write("<a href=\"github.html\">GitHub</a>");
document.write("<a href=\"gitignore.html\">Gitignore</a>");
document.write("<a href=\"internals.html\">Internals </a>");
document.write("<\/div>");

//document.write("<li class='active'><a href='home.html'><span>Home<\/span><\/a><\/li>");

/* <div class="topnav">
   <a class="active" href="#home">Home</a>
   <a href="#news">News</a>
   <a href="#contact">Contact</a>
   <a href="#about">About</a>
   </div>
   */
