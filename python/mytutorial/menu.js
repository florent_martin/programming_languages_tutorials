/* This script allows to display a fixed top menu bar.
I chosed to use a class for menu, but an id might be better.
*/

document.write("<div class=\"menu\">");
document.write("<a href=\"basics.html\">Basics</a>");
document.write("<a href='boosting_shap.html'>Boosting and shap</a>");
document.write("<a href=\"code_formatting.html\">Code formatting</a>");
document.write("<a href=\"context_manager.html\">Context manager</a>");
document.write("<a href=\"date.html\">Date and Time</a>");
document.write("<a href=\"debugger.html\">Debugger</a>");
document.write("<a href=\"documentation.html\">Documentation</a>");
document.write("<a href=\"h5py.html\">h5py</a>");
document.write("<a href=\"io.html\">IO and files</a>");
document.write("<a href=\"jupyter.html\">Jupyter</a>");
document.write("<a href=\"logging.html\">Logging</a>");
document.write("<a href=\"matplotlib.html\">Matplotlib</a>");
document.write("<a href=\"module.html\">Import, Modules, Packages</a>");
document.write("<a href=\"networkx.html\">NetworkX</a>");
document.write("<a href=\"nltk.html\">NLP and NLTK</a>");
document.write("<a href=\"numpy.html\">Numpy</a>");
document.write("<a href=\"OO.html\">OOP</a>");
document.write("<a href=\"packaging.html\">Packaging</a>");
document.write("<a href=\"pandas.html\">Pandas</a>");
document.write("<a href=\"profiling.html\">Profiling</a>");
document.write("<a href=\"regex.html\">Regex</a>");
document.write("<a href=\"scipy.html\">Scipy</a>");
document.write("<a href=\"sklearn.html\">Scikitlearn</a>");
document.write("<a href=\"testing.html\">Testing</a>");
document.write("<a href=\"xml.html\">HTML parsing</a>");
document.write("<\/div>");

//document.write("<li class='active'><a href='home.html'><span>Home<\/span><\/a><\/li>");

/* <div class="topnav">
  <a class="active" href="#home">Home</a>
    <a href="#news">News</a>
      <a href="#contact">Contact</a>
        <a href="#about">About</a>
	</div>
	*/
