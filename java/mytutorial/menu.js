/* This script allows to display a fixed top menu bar.
   I chosed to use a class for menu, but an id might be better.
   */

document.write("<div  class=\"menu\">");
document.write("<a href=\"index.html\">Home</a>");
document.write("<a href=\"basics.html\">Basics</a>");
document.write("<a href=\"class.html\">Class </a>");
document.write("<a href=\"compare.html\">Compare </a>");
document.write("<a href=\"compile.html\">Compile </a>");
document.write("<a href=\"enum_types.html\">Enum Type </a>");
document.write("<a href=\"filesIO.html\">Files and IO</a>");
document.write("<a href=\"generic_types.html\">Generic types </a>");
document.write("<a href=\"glossary.html\">Glossary </a>");
document.write("<a href=\"javadoc.html\">Javadoc </a>");
document.write("<a href=\"list.html\">List </a>");
document.write("<a href=\"new_object.html\">New Object </a>");
document.write("<a href=\"random.html\">Random </a>");
document.write("<a href=\"static.html\">Static </a>");
document.write("<a href=\"this.html\">This </a>");
document.write("<\/div>");

//document.write("<li class='active'><a href='home.html'><span>Home<\/span><\/a><\/li>");

/* <div class="topnav">
   <a class="active" href="#home">Home</a>
   <a href="#news">News</a>
   <a href="#contact">Contact</a>
   <a href="#about">About</a>
   </div>
   */
