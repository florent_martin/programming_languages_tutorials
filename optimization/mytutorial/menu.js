/* This script allows to display a fixed top menu bar.
   I chosed to use a class for menu, but an id might be better.
   */

document.write("<div  class=\"menu\">");
document.write("<a href=\"index.html\">Home</a>");
document.write("<a href=\"basics.html\">Basics</a>");
document.write("<a href=\"constraint_programming.html\">Constraint programming</a>");
document.write("<a href=\"file_formats.html\">File formats</a>");
document.write("<a href=\"gurobi.html\">Gurobi</a>");
document.write("<a href=\"GLPK.html\">GLPK</a>");
document.write("<a href=\"or_tools.html\">OR-tools</a>");
document.write("<a href=\"pyomo.html\">Pyomo</a>");
document.write("<a href=\"pure_optimization.html\">Pure Optimization</a>");
document.write("<\/div>");

//document.write("<li class='active'><a href='home.html'><span>Home<\/span><\/a><\/li>");

/* <div class="topnav">
   <a class="active" href="#home">Home</a>
   <a href="#news">News</a>
   <a href="#contact">Contact</a>
   <a href="#about">About</a>
   </div>
   */
