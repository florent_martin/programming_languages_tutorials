/* This script allows to display a fixed top menu bar.
I chosed to use a class for menu, but an id might be better.
*/

document.write("<div class=\"menu\">");
document.write("<a href=\"index.html\">Home</a>");
document.write("<a href=\"basics.html\">Basics</a>");
document.write("<a href=\"daemons.html\">Daemons</a>");
document.write("<a href=\"less.html\">Less</a>");
document.write("<a href=\"man.html\">Man pages</a>");
document.write("<a href=\"prompt.html\">Prompt</a>");
document.write("<a href=\"scripts.html\">Scripts and Programming</a>");
document.write("<a href=\"unix_directories.html\">Unix directories</a>");
document.write("<a href=\"users_security.html\">Users Security</a>");
document.write("<\/div>");

//document.write("<li class='active'><a href='home.html'><span>Home<\/span><\/a><\/li>");

/* <div class="topnav">
  <a class="active" href="#home">Home</a>
    <a href="#news">News</a>
      <a href="#contact">Contact</a>
        <a href="#about">About</a>
	</div>
	*/
