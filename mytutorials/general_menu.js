/* This script allows to display a fixed top menu bar.
I chosed to use a class for menu, but an id might be better.
*/

document.write("<div  id=\"general_menu\" class=\"menu\">");
document.write("<a href=\"../../architecture/mytutorial/basics.html\">Architecture</a>");
document.write("<a href=\"../../c/mytutorial/basics.html\">C</a>");
document.write("<a href=\"../../C++/mytutorial/basics.html\">C++</a>");
document.write("<a href=\"../../crypto/mytutorial/basics.html\">Crypto</a>");
document.write("<a href=\"../../data_science/mytutorial/basics.html\">Data science</a>");
document.write("<a href=\"../../dev_ops/mytutorial/basics.html\">Dev Ops</a>");
document.write("<a href=\"../../git/mytutorial/basics.html\">Git</a>");
document.write("<a href=\"../../java/mytutorial/basics.html\">Java</a>");
document.write("<a href=\"../../microsoft/mytutorial/basics.html\">Microsoft</a>");
document.write("<a href=\"../../optimization/mytutorial/basics.html\">Optimization</a>");
document.write("<a href=\"../../python/mytutorial/basics.html\">Python</a>");
document.write("<a href=\"../../r/mytutorial/basics.html\">R</a>");
document.write("<a href=\"../../sql/mytutorial/basics.html\">SQL</a>");
document.write("<a href=\"../../statistics/mytutorial/basics.html\">Statistics</a>");
document.write("<a href=\"../../tensorflow_keras/mytutorial/basics.html\">TensorFlow</a>");
document.write("<a href=\"../../unix/mytutorial/basics.html\">Unix</a>");
document.write("<a href=\"../../vim/mytutorial/basics.html\">Vim</a>");
document.write("<a href=\"../../visualization/mytutorial/basics.html\">Visualization</a>");
document.write("<a href=\"../../web_dev/mytutorial/basics.html\">Web dev</a>");
document.write("<\/div>");

//document.write("<li class='active'><a href='home.html'><span>Home<\/span><\/a><\/li>");

/* <div class="topnav">
  <a class="active" href="#home">Home</a>
    <a href="#news">News</a>
      <a href="#contact">Contact</a>
        <a href="#about">About</a>
	</div>
	*/
